# ChatGPTによるレビュー効率化検証

## プロジェクト情報

- アプリ
    - Java8/Maven/SpringBoot
- スクリプト
    - Python3/requests/openai

## プロジェクト構成

- src               ：Javaソース
- pom.xml           ：Javaビルド設定ファイル
- .gitlab-ci.yml    ：GitLab CI/CDパイプライン
- gitlab_review.py  ：マージリクエストの内容をOpenAIに投げて回答を取得するスクリプト
- requirements.txt  ：`gitlab_review.py`で使用するパッケージ

### 検証の流れ

1. GitLabのAPIトークンを作成し、GitLabのCI/CDの変数`GITLAB_ACCESS_TOKEN`に設定する。

1. OpenAIのアクセストークンを作成し、GitLabのCI/CDの変数`OPENAI_API_KEY`に設定する。

1. 作業ブランチを作成・修正を行い、マージリクエストを作成する。

1. `review`ステージはマニュアル実行となっているため、必要な時にパイプラインでブロックされているものを手動実行する。

1. 実行が終わるとマージリクエストにChatGPTから受け取ったレビュー内容をコメントとして追加する。

### その他

OpenAI APIはトークンの使用量で料金が発生する。トークンの展開は注意が必要。

- 日本語は英語に比べて約2倍となる模様。  
- 日本語の場合には大体`文字数×1.5`がトークンになりそう。  
- 入力も出力も両方お金が発生する。  
- モデルごとの2023/11/19時点の料金は以下の通り。

| model                     | input料金         | output料金          |
| ------------------------- | ----------------- | ------------------- |
| gpt-4-1106-preview        | $0.01/1K tokens   | $0.03/1K tokens     |
| gpt-4-1106-vision-preview | $0.01/1K tokens   | $0.03/1K tokens     |
| gpt-4                     | $0.03/1K tokens   | $0.06/1K tokens     |
| gpt-3.5-turbo-1106        | $0.0010/1K tokens | $0.0020/1K tokens   |
| gpt-3.5-turbo-instruct    | $0.0015/1K tokens | $0.0020 / 1K tokens |

